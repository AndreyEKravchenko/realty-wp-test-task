<?php
function prefix_add_footer_styles() {
	wp_enqueue_style( 'understrap-child-style', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'addrealty', get_stylesheet_directory_uri() . '/js/script.js' );
	 wp_localize_script( 'addrealty', 'understrapchildaddrealtyajax',
	  	array(
	'ajaxurl' => admin_url( 'admin-ajax.php' ),
	'otherParam' => 'some value',
) );
}
add_action( 'get_footer', 'prefix_add_footer_styles' );
add_action ( 'wp_ajax_add_realty', 'handler');
add_action ( 'wp_ajax_nopriv_add_realty', 'handler');

function handler() {
	$form_data = array();
	if (!empty( $_POST['form'])) {
		parse_str($_POST['form'], $form_data);
	}

	if ( ! wp_verify_nonce($form_data['_wpnonce'])) {
		wp_die("ERROR");
	}
	print_r($form_data);
	$new_realty = array(
		'post_title'    => $form_data['name'],
		'post_content'  => $form_data['description'],
		'post_status'   => 'publish',
		'post_type'     => 'realty',
		'post_parent'    => $form_data['chosecity'],
		'meta_input'   => array(
		'cost' => $form_data['price'],
	),
	);
	$realty_post_id = wp_insert_post( $new_realty );
	wp_set_object_terms( $realty_post_id, 5, 'realty' );
	wp_die();
}

add_action( 'init', 'register_post_types' );

function register_post_types(){
	register_post_type( 'realty', [
		'label'  => null,
		'labels' => [
			'name'               => 'Недвижимость',
			'singular_name'      => 'объект недвижимости',
			'add_new'            => 'Добавить объект',
			'add_new_item'       => 'Добавление объекта',
			'edit_item'          => 'Редактирование объекта',
			'new_item'           => 'Новый объект',
			'view_item'          => 'Смотреть объект',
			'search_items'       => 'Искать объект',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Недвижимость',
		],
		'description'            => '',
		'public'                 => true,
		'show_in_menu'           => null,
		'show_in_rest'        => null, 
		'rest_base'           => null,
		'menu_position'       => null,
		'menu_icon'           => null,
		'hierarchical'        => false,
		'supports'            => [ 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats' ],
		'taxonomies'          => ['Тип недвижимости'],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	] );

}

add_action( 'init', 'Create_tax_realty', 0 );

function Create_Tax_realty () 
{
    $args = array(
        'label' => _x( 'тип недвижимости', 'taxonomy general name' ),
        'labels' => array(
        'name' => _x( 'тип недвижимости', 'taxonomy general name' ),
        'singular_name' => _x( 'НЕдвижимость', 'taxonomy singular name' ),
        'menu_name' => __( 'Недвижимость' ),
        'all_items' => __( 'Вся недвижимость' ),
        'edit_item' => __( 'Изменить недвиждимость' ),
        'view_item' => __( 'Просмотреть недвижимость' ),
        'update_item' => __( 'Обновить недвижимость' ),
        'add_new_item' => __( 'Добавить новую недвижимость' ),
        'new_item_name' => __( 'Название' ),
        'parent_item' => __( 'Родительская' ),
        'parent_item_colon' => __( 'Родительская:' ),
        'search_items' => __( 'Поиск недвижимости' ),
        'popular_items' => null,
        'separate_items_with_commas' => null,
        'add_or_remove_items' => null,
        'choose_from_most_used' => null,
        'not_found' => __( 'Недвижимость не найдена.' ),
        ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_in_rest' => true,
        'meta_box_cb' => null,
        'show_admin_column' => false,
        'description' => '',
        'hierarchical' => true,
        'update_count_callback' => '',
        'query_var' => true,
        'rewrite' => array(
        'slug' => 'flat',
        'with_front' => false,
        'hierarchical' => true,
        'ep_mask' => EP_NONE,
    ),
        'sort' => null,
        '_builtin' => false,
    );
    register_taxonomy('typerealty', 'realty', $args);
}

add_action('add_meta_boxes', 'my_extra_fields', 1);
function my_extra_fields() {
	add_meta_box( 'extra_fields', 'Дополнительные поля', 'extra_fields_box_func', 'post', 'normal', 'high'  );
}

function extra_fields_box_func( $post ){
	?>
	<p><label><input type="text" name="extra[title]" value="<?php echo get_post_meta($post->ID, 'title', 1); ?>" style="width:50%" /> ? заголовок страницы (title)</label></p>
	<p>Описание статьи (description):
		<textarea type="text" name="extra[description]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'description', 1); ?></textarea>
	</p>

	<p>Видимость поста: <?php $mark_v = get_post_meta($post->ID, 'robotmeta', 1); ?>
		 <label><input type="radio" name="extra[robotmeta]" value="" <?php checked( $mark_v, '' ); ?> /> index,follow</label>
		 <label><input type="radio" name="extra[robotmeta]" value="nofollow" <?php checked( $mark_v, 'nofollow' ); ?> /> nofollow</label>
		 <label><input type="radio" name="extra[robotmeta]" value="noindex" <?php checked( $mark_v, 'noindex' ); ?> /> noindex</label>
		 <label><input type="radio" name="extra[robotmeta]" value="noindex,nofollow" <?php checked( $mark_v, 'noindex,nofollow' ); ?> /> noindex,nofollow</label>
	</p>

	<p><select name="extra[select]">
			<?php $sel_v = get_post_meta($post->ID, 'select', 1); ?>
			<option value="0">----</option>
			<option value="1" <?php selected( $sel_v, '1' )?> >Выбери меня</option>
			<option value="2" <?php selected( $sel_v, '2' )?> >Нет, меня</option>
			<option value="3" <?php selected( $sel_v, '3' )?> >Лучше меня</option>
		</select> ? выбор за вами</p>

	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
	<?php
}

add_action( 'save_post', 'my_extra_fields_update', 0 );

function my_extra_fields_update( $post_id ){
	// базовая проверка
	if (
		   empty( $_POST['extra'] )
		|| ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
		|| wp_is_post_autosave( $post_id )
		|| wp_is_post_revision( $post_id )
	)
		return false;

	$_POST['extra'] = array_map( 'sanitize_text_field', $_POST['extra'] );
	foreach( $_POST['extra'] as $key => $value ){
		if( empty($value) ){
			delete_post_meta( $post_id, $key );
			continue;
		}

		update_post_meta( $post_id, $key, $value );
	}
	return $post_id;
}

add_action( 'init', 'register_post_types_city' );

function register_post_types_city() {
	register_post_type( 'city', [
		'label'  => null,
		'labels' => [
			'name'               => 'Город',
			'singular_name'      => 'Город',
			'add_new'            => 'Добавить город',
			'add_new_item'       => 'Добавление города',
			'edit_item'          => 'Редактирование города',
			'new_item'           => 'Новый город',
			'view_item'          => 'Смотреть город',
			'search_items'       => 'Искать город',
			'not_found'          => 'Не найдено',
			'not_found_in_trash' => 'Не найдено в корзине',
			'parent_item_colon'  => '',
			'menu_name'          => 'Город',
		],
		'description'            => '',
		'public'                 => true,
		'show_in_menu'           => null,
		'show_in_rest'        => null, 
		'rest_base'           => null, 
		'menu_position'       => null,
		'menu_icon'           => null,
		'hierarchical'        => false,
		'supports'            => [ 'title', 'editor','page-attributes','post-formats' ,'thumbnail'],
		'taxonomies'          => [],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	] );
}

add_action('add_meta_boxes', function () {
	add_meta_box( 'city', 'city', 'city_metabox', 'realty', 'side', 'low'  );
}, 1);

function city_metabox( $post ){
	$cities = get_posts(array( 'post_type'=>'city', 'posts_per_page'=>-1, 'orderby'=>'post_title', 'order'=>'ASC' ));
	if( $cities ){
		echo '
		<div style="max-height:200px; overflow-y:auto;">
			<ul>
		';
		foreach( $cities as $city ){
			echo '
			<li><label>
				<input type="checkbox" name="post_parent" value="'. $city->ID .'" '. checked($city->ID, $post->post_parent, 0) .'> '. esc_html($city->post_title) .'
			</label></li>
			';
		}
		echo '
			</ul>
		</div>';
	}
	else
		echo 'empty';
}

add_action('add_meta_boxes', function(){
	add_meta_box( 'realty', 'Дома', 'realty_metabox', 'city', 'side', 'low'  );
}, 1);

function realty_metabox( $post ){
	$realtys = get_posts(array( 'post_type'=>'realty', 'post_parent'=>$post->ID, 'posts_per_page'=>-1, 'orderby'=>'post_title', 'order'=>'ASC' ));

	if( $realtys ){
		foreach( $realtys as $realty ){
			echo $realty->post_title .'<br>';
		}
	}
	else
		echo ' домов нет...';
}

add_theme_support( 'post-thumbnails' );
