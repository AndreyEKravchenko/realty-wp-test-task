<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<?php
			// Do the left sidebar check and open div#primary.
			get_template_part( 'global-templates/left-sidebar-check' );
			?>

			<main class="site-main" id="main">

				<?php
				while ( have_posts() ) {
					the_post();
					get_template_part( 'loop-templates/content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				}
				?>

			</main>

			<?php
			// Do the right sidebar check and close div#primary.
			get_template_part( 'global-templates/right-sidebar-check' );
			?>

		</div><!-- .row -->
<?php
		$query = new WP_Query( [
			'post_type'      => 'city',
			'posts_per_page' => 4		
		] );
		while ( $query->have_posts() ) : $query->the_post(); 
?>			
			<div class="item" style = "padding-bottom: 10px;">				
				<?php the_post_thumbnail( 'medium' ); ?>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br>	
			</div>
<?php 
		endwhile;
		wp_reset_query();
?>

<?php
		$query = new WP_Query( [
			'post_type'      => 'realty',
			'posts_per_page' => 4		
		] );
		while ( $query->have_posts() ) : $query->the_post(); 
?>
			<div class="item">				
				<?php the_post_thumbnail( 'medium' ); ?>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>	
			</div>
<?php 
		endwhile;
		wp_reset_query();
?>		
		<h3> Добавьте свою недвижимость</h3>
		<div class="add_realty">
			<div>
				<form action="" id="add_realty_form">
					<!-- <input type="hidden" name="action" value="add_realty"> -->
					<p><input id="name" name="name" type="text" placeholder="наименование недвижимости"><label for="name">Имя</label></p>
					<p><input id="description" name="description" type="textarea" placeholder="Описание"></p>
					<p><input id="price" name="price" type="number" placeholder="1000000"> рублей</p>
					<p><label for="chosecity">Выберите город из списка<br><select size="1" id="chosecity" name="chosecity">
	    					<option disabled>Выберите город</option>
	    					<option value="68">Ростов-на-Дoну</option>
	    					<option selected value="67">Москва</option>
	    					<option value="66">Калиниград</option>	    					
   						</select>
   						</label>
   					</p>
   					<p>
<!--    					<input type="file" multiple="multiple" accept=".txt,image/*">	
 -->   					</p>
<?php
					wp_nonce_field();
?>						
	   				<!-- <p><input type="submit" value="Загрузить" >
	   				</p> -->
	   				<button type="submit">Загрузить</button>
   				</form>
			</div>
		</div>

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php
get_footer();
