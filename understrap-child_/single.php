<?php
/**
 * The template for displaying all single posts
 *
 * @package Understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<?php
			// Do the left sidebar check and open div#primary.
			get_template_part( 'global-templates/left-sidebar-check' );
			?>

			<main class="site-main" id="main">

				<?php				
				while ( have_posts()) {					
					the_post();
					get_template_part( 'loop-templates/content', 'single' );
								
    				if ($post->post_type == "city" ) {
					 ?>
					 <div class="list city">
					 	<p> список недвижимости в городе <?php echo (get_the_title()); ?></p>

					 </div>
					 <div id="realtycontent">
					 	<div class="realtygrid">


			         	
			           
                    



<?php
						
						$realtys = get_posts(array( 'post_type'=>'realty', 'post_parent'=>$post->ID, 'posts_per_page'=>-1, 'orderby'=>'date', 'order'=>'ASC' ));

						if ( $realtys ) {
							$i = 0;
							foreach( $realtys as $realty ) {								
								if ( $i < 4 ) {
?>
									<div class="realtyitem">
			                			<div class="realtydiv">
<?php									
								 echo get_the_post_thumbnail( $realty->ID, 'thumbnail');
								
?>								 								
								<h2><a href="<?php the_permalink($realty); ?>"><?php echo get_the_title($realty) ?></a></h2>
<?php 
								//echo get_the_excerpt($realty);
?>
							</div>
			            </div>
<?php
								}
								$i++;
							}
						}
						else
							echo 'К сожалению в этом городе у нас нет недвижимости...';
					}
					
?>				                
			            </div>			 	
					 </div>
					 <?php
					 understrap_post_nav();
					// echo get_post_meta($post->ID, 'realty', true);
				}
				?>

			</main>

			<?php
			// Do the right sidebar check and close div#primary.
			get_template_part( 'global-templates/right-sidebar-check' );
			?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #single-wrapper -->

<?php
get_footer();
